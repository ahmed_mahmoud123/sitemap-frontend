import { Component, OnInit } from '@angular/core';
import { DashboardService } from '../../services';

@Component({
  selector: 'app-earning-chart',
  templateUrl: './earning-chart.component.html',
  styleUrls: ['./earning-chart.component.scss'],
})
export class EarningChartComponent implements OnInit {
  widget: any;

  isLoading = false;
  error: string;
  echartsInstance;
  result: Array<any> = [];

  options: any = {};
  constructor(private readonly __dashboardService: DashboardService) {}

  async getData() {
    try {
      this.isLoading = true;
      this.result = await this.__dashboardService.getRevenue();
      this.setOptions(this.result);
      requestAnimationFrame(() => {
        if (this.echartsInstance) {
          this.echartsInstance.resize();
        }
      });
      this.isLoading = false;
      this.error = null;
    } catch ({ error }) {
      this.error = error.message || 'Server Error';
    } finally {
      this.isLoading = false;
    }
  }
  async ngOnInit() {
    this.__dashboardService.queryChanged.subscribe(async (_) => {
      await this.getData();
    });
  }
  async onChartInit(ec) {
    this.echartsInstance = ec;
  }

  setOptions(result) {
    this.options = {
      color: ['#3398DB'],
      tooltip: {
        trigger: 'axis',
        axisPointer: {
          type: 'shadow',
        },
      },
      xAxis: [],
      yAxis: [],
      legend: {
        data: ['Organic Earnings', 'Total Earnings'],
      },
      series: [],
      grid: null,
    };
    if (result.length > 1) {
      this.options.grid = [{ bottom: '55%' }, { top: '55%' }];
    } else {
      this.options.grid = null;
    }

    result.forEach(({ result, organicResult }, index) => {
      this.options.xAxis.push({
        type: 'category',
        data: result.x,
        gridIndex: index,
      });
      this.options.yAxis.push({ gridIndex: index, type: 'value' });
      this.options.series = this.options.series.concat([
        {
          name: 'Organic Earnings',
          type: 'bar',
          barWidth: '60%',
          color: '#9b59b6',
          data: organicResult.revenue,
          xAxisIndex: index,
          yAxisIndex: index,
        },
        {
          name: 'Total Earnings',
          type: 'bar',
          barWidth: '60%',
          color: '#2980b9',
          data: result.revenue,
          xAxisIndex: index,
          yAxisIndex: index,
        },
      ]);
    });
  }
}

// implements OnInit {
//   isLoading = false;
//   error: string;
//   echartsInstance;
//   organicTotal: number;
//   total: number;

//   options = {
//     color: ['#3398DB'],
//     tooltip: {
//       trigger: 'axis',
//       axisPointer: {
//         type: 'shadow',
//       },
//     },
//     grid: {
//       left: '3%',
//       right: '4%',
//       bottom: '3%',
//       containLabel: true,
//     },
//     xAxis: [
//       {
//         type: 'category',
//         axisTick: {
//           alignWithLabel: true,
//         },
//         data: [],
//       },
//     ],
//     yAxis: [
//       {
//         type: 'value',
//       },
//     ],
//   };
//   merge;
//   constructor(
//     private readonly __overviewService: OverviewService,
//     private readonly __appService: AppService
//   ) {}

//   async getData() {
//     try {
//       this.isLoading = true;
//       const {
//         result,
//         organicTotal,
//         organicResult,
//         total,
//       } = await this.__overviewService.getRevenue();
//       this.organicTotal = organicTotal;
//       this.total = total;
//       this.setOptions(organicResult, result);
//       this.isLoading = false;
//       this.error = null;
//     } catch ({ error }) {
//       this.error = error.message || 'Server Error';
//     } finally {
//       this.isLoading = false;
//     }
//   }
//   async ngOnInit() {
//     this.__appService.changeProjct.subscribe(async () => {
//       await this.getData();
//     });
//   }
//   async onChartInit(ec) {
//     this.echartsInstance = ec;
//   }

//   setOptions(organicResult, res) {
//     this.merge = {
//       xAxis: [
//         // {
//         //   type: 'category',
//         //   axisTick: {
//         //     alignWithLabel: true,
//         //   },
//         //   data: organicResult.x,
//         // },
//         {
//           type: 'category',
//           axisTick: {
//             alignWithLabel: true,
//           },
//           data: res.x,
//         },
//       ],
//       legend: {
//         data: ['Organic Earnings', 'Total Earnings'],
//       },
//       series: [
//         {
//           name: 'Organic Earnings',
//           type: 'bar',
//           barWidth: '60%',
//           color: '#9b59b6',
//           data: organicResult.revenue,
//         },
//         {
//           name: 'Total Earnings',
//           type: 'bar',
//           barWidth: '60%',
//           color: '#2980b9',
//           data: res.revenue,
//         },
//       ],
//     };
//   }
// }
