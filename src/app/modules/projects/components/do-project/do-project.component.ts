import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { ProjectsService } from '../../services';
import { ActivatedRoute, Router } from '@angular/router';
import { AppService } from 'src/app/app.service';

@Component({
  selector: 'app-do-project',
  templateUrl: './do-project.component.html',
  styleUrls: ['./do-project.component.scss'],
})
export class DoProjectComponent implements OnInit {
  mode: 'ADD' | 'EDIT' = 'ADD';
  success: boolean;
  projectId: string;
  fail: boolean;
  isLoading = false;
  formGroup: FormGroup;
  properties;
  accuDomains;
  views;
  urls;
  currentProject: any;
  showUsers: boolean;
  constructor(
    private readonly __fb: FormBuilder,
    private readonly __projectService: ProjectsService,
    private readonly __activeRoute: ActivatedRoute,
    private readonly __appService: AppService,
    private readonly __router: Router
  ) {
    this.formGroup = this.__fb.group({
      sheetId: [''],
      image: [''],
      name: ['', Validators.required],
      description: ['', Validators.required],
      domain: [''],
      property: [''],
      viewId: [''],
      url: [''],
      _url: ['', Validators.required],
      csvFilename: [''],
      gsc: [false],
      csv: [''],
      accuranker: [false],
      accuDomain: ['', Validators.required],
      ga: [false],
      users: [],
    });
  }
  async ngOnInit(): Promise<void> {
    this.currentProject = this.__appService.getProjectFromLocalStorage();
    this.isLoading = true;
    const [urls, properties, accuDomains] = await Promise.all([
      this.__projectService.gscSites(),
      this.__projectService.gaProperties(),
      this.__projectService.accurDomain(),
    ]);
    this.urls = urls;
    this.properties = properties;
    this.accuDomains = accuDomains;
    this.accuDomain.setValue(accuDomains[0].id);
    this.isLoading = false;
    const projectId = this.__activeRoute.snapshot.params.id;
    if (projectId) {
      this.projectId = projectId;
      this.mode = 'EDIT';
      const project = await this.__projectService.getProjectById(projectId);
      this.showUsers = true;
      this.formGroup.patchValue({ ...project });
      this.changeProperty(false);
    } else {
      this.showUsers = true;
      this.mode = 'ADD';
    }
  }
  get ga(): FormControl {
    return this.formGroup.controls.ga as FormControl;
  }

  get csvFilename(): FormControl {
    return this.formGroup.controls.csvFilename as FormControl;
  }

  get users(): FormControl {
    return this.formGroup.controls.users as FormControl;
  }

  get domain(): FormControl {
    return this.formGroup.controls.domain as FormControl;
  }

  get csv(): FormControl {
    return this.formGroup.controls.csv as FormControl;
  }

  get image(): FormControl {
    return this.formGroup.controls.image as FormControl;
  }

  get name(): FormControl {
    return this.formGroup.controls.name as FormControl;
  }

  get description(): FormControl {
    return this.formGroup.controls.description as FormControl;
  }

  get viewId(): FormControl {
    return this.formGroup.controls.viewId as FormControl;
  }

  get url(): FormControl {
    return this.formGroup.controls.url as FormControl;
  }

  get _url(): FormControl {
    return this.formGroup.controls._url as FormControl;
  }

  get gsc(): FormControl {
    return this.formGroup.controls.gsc as FormControl;
  }
  get accuranker(): FormControl {
    return this.formGroup.controls.accuranker as FormControl;
  }
  get accuDomain(): FormControl {
    return this.formGroup.controls.accuDomain as FormControl;
  }

  get property(): FormControl {
    return this.formGroup.controls.property as FormControl;
  }

  async changeProperty(setValue = true) {
    if (!this.property.value) {
      return;
    }
    this.isLoading = true;
    const { id, accountId } = this.properties.find(
      (p) => p.id === this.property.value
    );

    this.views = await this.__projectService.gaViewOfProperty(id, accountId);
    if (setValue) this.viewId.setValue(this.views[0].id);
    this.isLoading = false;
  }
  toggelGa() {
    if (this.ga.value) {
      this.property.setValidators([Validators.required]);
      this.viewId.setValidators([Validators.required]);
      this.property.setValue(this.properties[0].id);
      this.changeProperty();
    } else {
      this.property.setValidators([]);
      this.viewId.setValidators([]);
      this.formGroup.patchValue({ property: null, viewId: null });
    }
  }
  toggelGoogleConsole() {
    if (this.gsc.value) {
      this.url.setValidators([Validators.required]);
      this.url.setValue(this.urls[0].siteUrl);
    } else {
      this.url.setValidators([]);
      this.formGroup.patchValue({ url: null });
    }
  }
  toggelAccuranker() {
    if (this.accuranker.value) {
      this.accuDomain.setValidators([Validators.required]);
      this.accuDomain.setValue(this.accuDomains[0].id);
    } else {
      this.url.setValidators([]);
      this.formGroup.patchValue({ accuDomain: null });
    }
  }

  urlChanged(url) {
    if (url) {
      if (this.accuDomains) {
        const acc = this.accuDomains.find(
          (item) => item.domain.toLowerCase().indexOf(url.toLowerCase()) >= 0
        );
        if (acc) {
          this.formGroup.patchValue({ accuDomain: acc.id });
        }
      }
      if (this.properties) {
        const property = this.properties.find(
          (item) => item.name.toLowerCase().indexOf(url.toLowerCase()) >= 0
        );
        if (property) {
          this.formGroup.patchValue({ property: property.id });
          this.changeProperty(true);
        }
      }
      if (this.urls) {
        const _url = this.urls.find(
          (item) => item.siteUrl.toLowerCase().indexOf(url.toLowerCase()) >= 0
        );
        if (_url) {
          this.formGroup.patchValue({ url: _url.siteUrl });
        }
      }
    }
  }

  loadImage({ target: { files } }) {
    if (files && files[0]) {
      var FR = new FileReader();
      FR.onload = (e) => {
        this.image.setValue(e.target.result);
      };
      FR.readAsDataURL(files[0]);
    }
  }

  loadCSV({ target: { files } }) {
    if (files && files[0]) {
      var FR = new FileReader();
      FR.onload = (e) => {
        console.log({ file: files[0] });
        this.csvFilename.setValue(files[0].name);
        this.csv.setValue(btoa(e.target.result as string));
      };
      FR.readAsBinaryString(files[0]);
    } else {
      this.csvFilename.setValue(null);
      this.csv.setValue(null);
    }
  }

  async save() {
    try {
      this.success = this.fail = false;
      if (this.formGroup.valid) {
        this.isLoading = true;
        if (this.mode === 'ADD') {
          const project = await this.__projectService.save(
            this.formGroup.value
          );
          requestAnimationFrame(() => this.selectProject(project));
          return;
        } else if (this.mode === 'EDIT') {
          await this.__projectService.updateProject(
            this.__activeRoute.snapshot.params.id,
            this.formGroup.value
          );
        }
        this.isLoading = false;
        this.success = true;

        if (this.currentProject?.viewId === this.formGroup.value.viewId) {
          localStorage.setItem('project', JSON.stringify(this.formGroup.value));
          this.__appService.changeProjct.emit(true);
        }

        this.__appService.projectsChaged.emit(true);
        document.getElementById('top').scrollTo({
          top: 0,
          left: 0,
          behavior: 'smooth',
        });
      }
    } catch (err) {
      this.isLoading = false;
      this.fail = true;
      this.success = false;
    }
  }

  selectProject(project) {
    localStorage.setItem('project', JSON.stringify(project));
    localStorage.setItem('viewId', project.viewId);
    localStorage.setItem('projectId', project._id);
    localStorage.setItem('projectName', project.name);
    this.__appService.changeProjct.emit(true);
    this.__router.navigate(['/main/overview']);
  }
}
