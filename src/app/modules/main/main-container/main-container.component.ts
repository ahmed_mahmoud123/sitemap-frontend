import { Component, OnInit } from '@angular/core';
import { AppService } from 'src/app/app.service';
import { UsersService } from '../../users/services';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main-container',
  templateUrl: './main-container.component.html',
  styleUrls: ['./main-container.component.scss'],
})
export class MainContainerComponent implements OnInit {
  project: any;
  user: any;
  constructor(
    private readonly __appService: AppService,
    private readonly __usersService: UsersService,
    private readonly __router: Router
  ) {}
  projectName = localStorage.getItem('projectName');

  async ngOnInit(): Promise<void> {
    try {
      this.user = this.__appService.getUserFromLocalStorage();
      this.user.id = this.user.id || this.user._id;
      this.user = Object.assign(
        this.user,
        await this.__usersService.getUserById(this.user.id)
      );
      localStorage.setItem('user', JSON.stringify(this.user));
      this.__appService.changeProjct.subscribe(() => {
        this.projectName = localStorage.getItem('projectName');
        this.project = this.__appService.getProjectFromLocalStorage();
      });
    } catch (err) {
      localStorage.clear();
      this.__router.navigate(['/']);
    }
  }
}
